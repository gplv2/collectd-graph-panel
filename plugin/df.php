<?php

# Collectd Df plugin

require_once 'conf/common.inc.php';
require_once 'type/GenericStacked.class.php';
require_once 'inc/collectd.inc.php';

# LAYOUT
#
# df-X/df_complex-free.rrd
# df-X/df_complex-reserved.rrd
# df-X/df_complex-used.rrd
/*
/var/lib/collectd/rrd/server20/df-boot:
total 464
drwxr-xr-x  2 root root   4096 Apr  1 15:48 ./
drwxr-xr-x 42 root root   4096 Apr  1 15:50 ../
-rw-r--r--  1 root root 154888 Apr  6 18:18 df_complex-free.rrd
-rw-r--r--  1 root root 154888 Apr  6 18:19 df_complex-reserved.rrd
-rw-r--r--  1 root root 154888 Apr  6 18:19 df_complex-used.rrd

/var/lib/collectd/rrd/server20/df-root:
total 464
drwxr-xr-x  2 root root   4096 Apr  1 15:48 ./
drwxr-xr-x 42 root root   4096 Apr  1 15:50 ../
-rw-r--r--  1 root root 154888 Apr  6 18:19 df_complex-free.rrd
-rw-r--r--  1 root root 154888 Apr  6 18:19 df_complex-reserved.rrd
-rw-r--r--  1 root root 154888 Apr  6 18:19 df_complex-used.rrd

/var/lib/collectd/rrd/server21/df:
total 312
drwxr-xr-x  2 root root   4096 Apr  2 10:05 ./
drwxr-xr-x 33 root root   4096 Apr  2 10:08 ../
-rw-r--r--  1 root root 307712 Apr  6 18:19 df-root.rrd
*/

$obj = new Type_GenericStacked($CONFIG);
$obj->data_sources = array('value');
$obj->order = array('reserved', 'free', 'used');
$obj->ds_names = array(
	'reserved' => 'Reserved',
	'free' => 'Free',
	'used' => 'Used',
);
$obj->colors = array(
	'reserved' => 'aaaaaa',
	'free' => '00ff00',
	'used' => 'ff0000',
);
$obj->width = $width;
$obj->heigth = $heigth;

$obj->rrd_title = sprintf('Free space (%s)', $obj->args['pinstance']);
$obj->rrd_vertical = 'Bytes';
$obj->rrd_format = '%5.1lf%sB';

# backwards compatibility
if ($CONFIG['version'] < 5) {
	$obj->data_sources = array('free', 'used');
	$obj->rrd_title = sprintf('Free space (%s)', $obj->args['tinstance']);
}

echo "<pre>";
collectd_flush($obj->identifiers);
$obj->rrd_graph();
var_dump($obj);
echo "</pre>"; exit;
